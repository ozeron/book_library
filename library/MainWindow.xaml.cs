﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Configuration;
using database_lib.Helpers;

namespace library
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // MS SQL Server Connection String
            string SQL_CONNECTION_STRING = ConfigurationManager.
                ConnectionStrings["DBLibrarySqlConnectionStrings"].ConnectionString;

            PublisherDbHelper helper = new PublisherDbHelper(SQL_CONNECTION_STRING);

            var list = helper.GetPublisherById(1);
        }
    }
}
